package com.apobates.jforum.comments;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseTest {
    protected static List<Comments> source;
    protected static List<CommentsRelations> relations;
    //初始数据
    static {
        //一级
        Comments c1 = new Comments(1, "xiaofanku", 1, "这个新闻说的好", true);
        Comments c2 = new Comments(3, "renhelan", 3, "我也觉得说的好", true);
        Comments c3 = new Comments(8, "ghy", 8, "你们说的撒呀", false);
        //other one layer
        Comments c4 = new Comments(2, "liri", 2, "我觉得不好", false);
        CommentsRelations cr4 = new CommentsRelations("1", 2);
        //-
        Comments c5 = new Comments(4, "heze", 4, "呵呵", false);
        CommentsRelations cr5 = new CommentsRelations("1", 4);
        //-
        Comments c6 = new Comments(5, "jinan", 5, "呵呵哒", false);
        CommentsRelations cr6 = new CommentsRelations("1", 5);
        //-
        Comments c7 = new Comments(6, "wuhu", 6, "那是你有问题", false);
        CommentsRelations cr7 = new CommentsRelations("1", 6);
        //-
        Comments c8 = new Comments(7, "london", 7, "+1", true);
        CommentsRelations cr8 = new CommentsRelations("3", 7);
        //-
        Comments c9 = new Comments(9, "beijin", 9, "我觉得不好", false);
        CommentsRelations cr9 = new CommentsRelations("7,3", 9);
        //-
        Comments c10 = new Comments(10, "yantai", 10, "呵呵", true);
        CommentsRelations cr10 = new CommentsRelations("7,3", 10);
        //-
        Comments c11 = new Comments(11, "weihai", 11, "回复功能等", true);
        CommentsRelations cr11 = new CommentsRelations("10,7,3", 11);
        //-
        Comments c12 = new Comments(12, "apple", 12, "再一次的回眸", true);
        CommentsRelations cr12 = new CommentsRelations("11,10,7,3", 12);
        //-
        Comments c13 = new Comments(13, "nokia", 13, "蚌埔", false);
        CommentsRelations cr13 = new CommentsRelations("10,7,3", 13);
        //-
        Comments c14 = new Comments(14, "oppo", 14, "秀儿", false);
        CommentsRelations cr14 = new CommentsRelations("12,11,10,7,3", 14);
        //
        source = Stream
                .of(c1, c2, c3, c4, c5, c6, c7, c8, c9, c10, c11, c12, c13, c14)
                .sorted(Comparator.comparingInt(Comments::getFloor))
                .collect(Collectors.toList());
        relations = Arrays.asList(cr4, cr5, cr6, cr7, cr8, cr9, cr10, cr11, cr12, cr13, cr14);
    }
}
