package com.apobates.jforum.comments;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TreeTest {
    public static void main(String[] args) {
        CommentsRelations cr4 = new CommentsRelations("1", 2);
        CommentsRelations cr5 = new CommentsRelations("1", 4);
        CommentsRelations cr6 = new CommentsRelations("1", 5);
        CommentsRelations cr7 = new CommentsRelations("1", 6);
        CommentsRelations cr8 = new CommentsRelations("3", 7);
        CommentsRelations cr9 = new CommentsRelations("7,3", 9);
        CommentsRelations cr10 = new CommentsRelations("7,3", 10);
        CommentsRelations cr11 = new CommentsRelations("10,7,3", 11);
        List<CommentsRelations> relations = Arrays.asList(cr4, cr5, cr6, cr7, cr8, cr9, cr10, cr11);
        TreeSet<Integer> a = buildRelationTree(1, relations);
        Iterator<Integer> iterator = a.iterator();
        while (iterator.hasNext()){
            System.out.println("node="+iterator.next()); //3-7-9-10-11
        }
    }
    /**
     * 构建谁的树
     * @param parentCommentId 父节点
     * @param relations
     * @return
     */
    private static TreeSet<Integer> buildRelationTree(Integer parentCommentId, List<CommentsRelations> relations){
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };
        TreeSet<Integer> commentIdTree = new TreeSet<>(comparator);
        for(CommentsRelations crs : relations){
            TreeSet<Integer> tmp = convert(crs.getSequence());
            if(tmp.contains(parentCommentId)) {
                commentIdTree.add(crs.getCommentsId());
                commentIdTree.addAll(tmp);
            }
        }
        return commentIdTree;
    }

    /**
     * 将评论的引用关系转成引用的评论ID集合
     *
     * @param commentIds
     * @return
     */
    private static TreeSet<Integer> convert(String commentIds){
        return Stream.of(commentIds.split(",")).map(Integer::valueOf).collect(
                Collectors.toCollection(TreeSet::new)
        );
    }
}
