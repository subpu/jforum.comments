package com.apobates.jforum.comments;

import com.apobates.jforum.comments.core.CommentsConverter;
import com.apobates.jforum.comments.core.TiebaStyleCommentsConverter;
import com.apobates.jforum.comments.vo.NewsReplyItem;
import java.util.Comparator;
import java.util.List;

/**
 * 新闻评论模式测试.深度参数不能小于1
 */
public class NewsTest extends BaseTest{
    public static void main(String[] args) {
        //---------------------------------------------------------------
        // [1]这个新闻说的好(1)
        //        -->我觉得不好(2.1)-->
        //        -->呵呵(4.1)-->
        //        -->呵呵哒(5.1)-->
        //        -->那是你有问题(6.1)-->
        //
        //---------------------------------------------------------------
        // [3]我也觉得说的好(3)
        //        -->+1(7.3)
        //        -->我觉得不好(9)-->+1(7.3)
        //        -->呵呵(10)-->+1(7.3)
        //        -->回复功能等(11)-->呵呵(10)-->+1(7.3)
        //        -->再一次的回眸(12)-->回复功能等(11)-->呵呵(10)-->+1(7.3)
        //---------------------------------------------------------------
        // [8]你们说的撒呀(8)
        //---------------------------------------------------------------
        CommentsConverter converter = new TiebaStyleCommentsConverter(3);
        List<NewsReplyItem> rs = converter.map(source, relations);
        rs.stream().sorted(Comparator.comparingInt(NewsReplyItem::getRanking)).forEach(cv->{
            String t = "[%d]%s say: %s\r\n%s";
            StringBuffer repliesContent = new StringBuffer();
            for (NewsReplyItem tmp : cv.getNodes()){ //一级
                repliesContent.append("->"+tmp.getUser()+": "+tmp.getContent()+"\r\n");
                for(NewsReplyItem ttmp : tmp.getNodes()){ //二级
                    repliesContent.append("->->"+ttmp.getUser()+": "+ttmp.getContent()+"\r\n");
                    for (NewsReplyItem tttmp : ttmp.getNodes()){ //三级
                        repliesContent.append("->->->"+tttmp.getUser()+": "+tttmp.getContent()+"\r\n");
                        for(NewsReplyItem foldIns : tttmp.getNodes()){ //折叠更深层的评论进第四层
                            repliesContent.append("->->->->"+foldIns.getUser()+": "+foldIns.getContent()+"\r\n");
                        }
                    }
                }
            }
            System.out.println(String.format(t, cv.getRanking(), cv.getUser(), cv.getContent(), repliesContent.toString()));
        });
    }
}
