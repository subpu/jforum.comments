package com.apobates.jforum.comments;

import com.apobates.jforum.comments.core.CommentsConverter;
import com.apobates.jforum.comments.core.ForumStyleCommentsConverter;
import com.apobates.jforum.comments.vo.ThreadsCommentsVo;
import java.util.Comparator;
import java.util.List;

/**
 * 论坛blockquote引用模式测试
 */
public class ThreadsTest extends BaseTest{
    public static void main(String[] args) {
        //---------------------------------------------------------------
        // [1]这个新闻说的好(1)
        //        -->我觉得不好(2.1)-->
        //        -->呵呵(4.1)-->
        //        -->呵呵哒(5.1)-->
        //        -->那是你有问题(6.1)-->
        //
        //---------------------------------------------------------------
        // [3]我也觉得说的好(3)
        //        -->+1(7.3)
        //        -->我觉得不好(9)-->+1(7.3)
        //        -->呵呵(10)-->+1(7.3)
        //        -->回复功能等(11)-->呵呵(10)-->+1(7.3)
        //        -->再一次的回眸(12)-->回复功能等(11)-->呵呵(10)-->+1(7.3)
        //---------------------------------------------------------------
        // [8]你们说的撒呀(8)
        //---------------------------------------------------------------
        CommentsConverter converter = new ForumStyleCommentsConverter();
        List<ThreadsCommentsVo> rs = converter.map(source, relations);
        rs.stream().sorted(Comparator.comparingInt(ThreadsCommentsVo::getRanking)).forEach(cv->{
            String t = "[%d]%s say: %s";
            System.out.println(String.format(t, cv.getRanking(), cv.getUser(), cv.getBlockQuotes()));
        });
    }
}
