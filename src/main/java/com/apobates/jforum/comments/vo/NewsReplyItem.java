package com.apobates.jforum.comments.vo;

import com.apobates.jforum.comments.Comments;
import java.util.ArrayList;
import java.util.List;
/**
 * 百度Tieba的回复值对象
 */
public class NewsReplyItem {
    private String user;
    private String content;
    private int ranking;
    private List<NewsReplyItem> nodes;

    public NewsReplyItem() {
    }
    //一级评论
    public NewsReplyItem(Comments comments){
        this.user = comments.getUser();
        this.content = comments.getContent();
        this.ranking = comments.getFloor();
        this.nodes = new ArrayList<>();
    }
    //折叠时使用
    public NewsReplyItem(String user, Comments comments){
        this.user = user;
        this.content = comments.getContent();
        this.ranking = comments.getFloor();
        this.nodes = new ArrayList<>();
    }
    //递归时使用
    public NewsReplyItem(Comments comments, List<NewsReplyItem> nodes){
        this.user = comments.getUser();
        this.content = comments.getContent();
        this.ranking = comments.getFloor();
        this.nodes = nodes;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }

    public List<NewsReplyItem> getNodes() {
        return nodes;
    }

    public void setNodes(List<NewsReplyItem> nodes) {
        this.nodes = nodes;
    }
}
