package com.apobates.jforum.comments.vo;

/**
 * 评论的论坛回复值对象
 */
public class ThreadsCommentsVo {
    private String user;
    private String blockQuotes;
    private int ranking;

    public ThreadsCommentsVo() {
    }

    public ThreadsCommentsVo(int ranking, String user, String blockQuotes) {
        this.ranking = ranking;
        this.blockQuotes = blockQuotes;
        this.user = user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getBlockQuotes() {
        return blockQuotes;
    }

    public void setBlockQuotes(String blockQuotes) {
        this.blockQuotes = blockQuotes;
    }

    public int getRanking() {
        return ranking;
    }

    public void setRanking(int ranking) {
        this.ranking = ranking;
    }
}
