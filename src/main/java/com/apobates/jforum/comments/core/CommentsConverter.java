package com.apobates.jforum.comments.core;

import com.apobates.jforum.comments.Comments;
import com.apobates.jforum.comments.CommentsRelations;
import java.util.List;

/**
 * 评论视图转换器
 * @param <T>
 */
public interface CommentsConverter<T> {
    /**
     * 评论映射成指定视图对象
     * @param rs 评论
     * @param relations 评论的引用关系
     * @return
     */
    List<T> map(List<Comments> rs, List<CommentsRelations> relations);
}
