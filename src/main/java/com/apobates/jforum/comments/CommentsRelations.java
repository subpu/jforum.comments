package com.apobates.jforum.comments;

/**
 * 评论的引用关系
 */
public class CommentsRelations {
    //回复序列,最近引用在前. 例: 3,2,1
    private String sequence;
    //回复ID. 例: 4
    private int commentsId;

    public String getSequence() {
        return sequence;
    }

    public void setSequence(String sequence) {
        this.sequence = sequence;
    }

    public int getCommentsId() {
        return commentsId;
    }

    public void setCommentsId(int commentsId) {
        this.commentsId = commentsId;
    }

    public CommentsRelations() {
    }

    public CommentsRelations(String sequence, int commentsId) {
        this.sequence = sequence;
        this.commentsId = commentsId;
    }
}
