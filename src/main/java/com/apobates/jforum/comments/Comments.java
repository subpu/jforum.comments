package com.apobates.jforum.comments;

/**
 * 评论
 */
public class Comments implements java.lang.Comparable<Comments>{
    //回复ID
    private int id;
    //作者
    private String user;
    //楼层
    private int floor;
    //回复内容
    private String content;
    //是否被引用了
    private boolean hasQuote;

    public Comments() {
    }

    public Comments(int id, String user, int floor, String content, boolean hasQuote) {
        this.id = id;
        this.user = user;
        this.floor = floor;
        this.content = content;
        this.hasQuote = hasQuote;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isHasQuote() {
        return hasQuote;
    }

    public void setHasQuote(boolean hasQuote) {
        this.hasQuote = hasQuote;
    }

    @Override
    public int compareTo(Comments o) {
        return getId() - o.getId();
    }
}
