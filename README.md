# jforum.comments

#### 介绍
评论盖楼示例

#### 表结构

Comments 评论表

| id | user | floor | content | hasQuote |
| :-: | :-: | :-: | :-: | :-: |
| 主键 | 评论作者 | 楼层 | 评论内容 | 是否被回复 |
| Integer | String | Integer | String | boolean |

CommentsRelations 回复关系表

| sequence | commentsId |
| :-: | :-: |
| 回复序列 | 评论ID |
| String | Integer |

#### CommentsConverter 视图转换器

1.  ForumStyleCommentsConverter 传统的论坛BlockQuote引用风格

    ThreadsCommentsVo ：视图值对象

2.  TiebaStyleCommentsConverter 百度贴吧回复风格

    NewsReplyItem :  视图值对象

    最低的递归深度:1, TiebaStyleCommentsConverter 默认构造器的深度值:3


#### 测试示例

1.  NewsTest 贴吧回复风格的执行类

2.  ThreadsTest 论坛BlockQuote引用风格的执行类

3.  BaseTest 初始测试数据填充

#### NewsTest 截图

1.  深度1
![输入图片说明](https://images.gitee.com/uploads/images/2021/0912/164155_303b07d3_351732.png "1.png")

2.  深度2
![输入图片说明](https://images.gitee.com/uploads/images/2021/0912/164141_a46701f3_351732.png "2.png")

3.  深度3
![输入图片说明](https://images.gitee.com/uploads/images/2021/0912/164128_39c7e126_351732.png "3.png")




